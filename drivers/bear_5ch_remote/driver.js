'use strict';

const Homey = require('homey');

class Bear5chRemoteDriver extends Homey.Driver {
    onPairListDevices(data, callback) {
        callback(null, [
            {
                name: 'Bear Channel 1',
                data: {
                    id: 'bear_channel_1'
                }
            },
            {
                name: 'Bear Channel 2',
                data: {
                    id: 'bear_channel_2'
                }
            },
            {
                name: 'Bear Channel 3',
                data: {
                    id: 'bear_channel_3'
                }
            },
            {
                name: 'Bear Channel 4',
                data: {
                    id: 'bear_channel_4'
                }
            },
            {
                name: 'Bear Channel 5',
                data: {
                    id: 'bear_channel_5'
                }
            }
        ]);
    }
}

module.exports = Bear5chRemoteDriver;
