'use strict';

const Homey = require('homey');

const CH_1_UP = [ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0 ];
const CH_1_DN = [ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1 ];
const CH_1_ST = [ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0 ];

const CH_2_UP = [ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0 ];
const CH_2_DN = [ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1 ];
const CH_2_ST = [ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0 ];

const CH_3_UP = [ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0 ];
const CH_3_DN = [ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1 ];
const CH_3_ST = [ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0 ];

const CH_4_UP = [ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0 ];
const CH_4_DN = [ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1 ];
const CH_4_ST = [ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0 ];

const CH_5_UP = [ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0 ];
const CH_5_DN = [ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1 ];
const CH_5_ST = [ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0 ];

const CH_A_UP = [ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0 ];
const CH_A_DN = [ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1 ];
const CH_A_ST = [ 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0 ];

class Bear5chRemoteDevice extends Homey.Device {
    // This method is called when the Device is initiated
    onInit() {
        this.bearRemoteSignal = new Homey.Signal433('bear');
        this.bearRemoteSignal.register().catch(this.error);

        // Register the callback to hadle state changes
        this.registerCapabilityListener('windowcoverings_state', this.onCapabilityWindowCoveringsState.bind(this));
    }

    onCapabilityWindowCoveringsState(value, opts, callback) {
        if (this.bearRemoteSignal !== undefined) {
            let data = this.getData();

            if (data.id == 'bear_channel_1' && value == 'up') {
                this.bearRemoteSignal.tx(CH_1_UP);
            }
            else if (data.id == 'bear_channel_1' && value == 'down') {
                this.bearRemoteSignal.tx(CH_1_DN);
            }
            else if (data.id == 'bear_channel_1' && value == 'idle') {
                this.bearRemoteSignal.tx(CH_1_ST);
            }
            else if (data.id == 'bear_channel_2' && value == 'up') {
                this.bearRemoteSignal.tx(CH_2_UP);
            }
            else if (data.id == 'bear_channel_2' && value == 'down') {
                this.bearRemoteSignal.tx(CH_2_DN);
            }
            else if (data.id == 'bear_channel_2' && value == 'idle') {
                this.bearRemoteSignal.tx(CH_2_ST);
            }
            else if (data.id == 'bear_channel_3' && value == 'up') {
                this.bearRemoteSignal.tx(CH_3_UP);
            }
            else if (data.id == 'bear_channel_3' && value == 'down') {
                this.bearRemoteSignal.tx(CH_3_DN);
            }
            else if (data.id == 'bear_channel_3' && value == 'idle') {
                this.bearRemoteSignal.tx(CH_3_ST);
            }
            else if (data.id == 'bear_channel_4' && value == 'up') {
                this.bearRemoteSignal.tx(CH_4_UP);
            }
            else if (data.id == 'bear_channel_4' && value == 'down') {
                this.bearRemoteSignal.tx(CH_4_DN);
            }
            else if (data.id == 'bear_channel_4' && value == 'idle') {
                this.bearRemoteSignal.tx(CH_4_ST);
            }
            else if (data.id == 'bear_channel_5' && value == 'up') {
                this.bearRemoteSignal.tx(CH_5_UP);
            }
            else if (data.id == 'bear_channel_5' && value == 'down') {
                this.bearRemoteSignal.tx(CH_5_DN);
            }
            else if (data.id == 'bear_channel_5' && value == 'idle') {
                this.bearRemoteSignal.tx(CH_5_ST);
            }
            else {
                return Promise.reject(new Error('Unknown command.'));
            }
        }
        else {
            return Promise.reject(new Error('Signal not ready.'));
        }

        callback(null);
    }
}

module.exports = Bear5chRemoteDevice;
