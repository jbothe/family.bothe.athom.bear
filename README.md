# Bear Blind and Awning Automation

I have motorized blinds at home controlled by a remote with 'Bear' written on it, here are the details: http://www.awningsystems.com.au/sites/default/files/Product%20Specification%20Bear%20Motors.pdf

This driver adds support for blinds and awnings controlled with a Bear automation system.

What works:

* The Bear 5 channel remote is supported

Todo:

* Support for other Bear remotes (e.g. the 1 and 15 channel remote)
