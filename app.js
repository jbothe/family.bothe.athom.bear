'use strict';

const Homey = require('homey');

class BearApp extends Homey.App {
    onInit() {
        this.log('Bear is running...');
    }
}

module.exports = BearApp;
